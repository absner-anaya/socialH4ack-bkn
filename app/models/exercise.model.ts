
import { mongoose } from '../config/mongoose.config';
import { Schema, Document, Model } from "mongoose";

export interface IExercise extends Document {
    name?: String,
    typeExercise?: String,
    mainImage?: String,
    levelDifficulty?: Number,
    timer?: {
        duration?: Number
    },
    lights?: [{
        colorCode?: String
    }],
    music?: [
        {
            name?: String,
            sourcePath?: String,
            volumen?: Number
        }
    ],
    videoTutor?: [
        {
            name?: String,
            sourcePath?: String
        }
    ]
    createdBy?: String,
    status?: String,
    creationDate?: Date,
    updated?: Date
}

const schema    =   new Schema({
    name: String,
    typeExercise: String,
    mainImage: String,
    levelDifficulty: Number,
    timer: {
        duration: Number
    },
    lights: [{
        colorCode: String
    }],
    music: [
        {
            name: String,
            sourcePath: String,
            volumen: Number
        }
    ],
    videoTutor: [
        {
            name: String,
            sourcePath: String
        }
    ],
    createdBy: String,
    creationDate: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

schema.index({ "name" : "text" });
export interface IExerciseModel {
}
export type ExerciseModel = Model<IExercise> & IExerciseModel;
export const Exercise : ExerciseModel = <ExerciseModel>mongoose.model<IExercise>("Exercise", schema);