import { NextFunction, Request, Response, Router } from "express";
import { UserController } from '../controllers/user.controller';

export class IndexRouter {

    constructor(){

    }

    public static create(router: Router){

        router.get('/exercise', (req: Request, res: Response)  =>  {
            new UserController().find(req,res);
        });
        router.get('/exercise/:id', (req: Request, res: Response) => {
            new UserController().findById(req,res);
        });
        router.post('/exercise', (req: Request, res: Response)  =>  {
            new UserController().create(req,res);
        });
        router.put('/exercise/:id', (req: Request, res: Response) => {
            new UserController().update(req,res);
        });
        router.delete('/exercise/:id', (req: Request, res: Response) => {
            new UserController().destroy(req,res);
        })
        
        
    }
}